/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cotizacion;

/**
 *
 * @author je270
 */
public class Cotizacion {
    //atributos de la clase
    public int numCotizacion;
    public String desAuto;
    public float precio;
    public float pagoInicial;
    public int plazo;
    
    //metodos
    //metodos constructores
    //métodos por omisión
    
    public Cotizacion(){
    this.numCotizacion=0;
    this.desAuto="";
    this.precio=0.0f;
    this.pagoInicial=0.0f;
    this.plazo=0;
    }
    //argumentos
    public Cotizacion(int numCotizacion, float precio, String desAuto, float pagoInicial, int plazo){
    this.numCotizacion=numCotizacion;
    this.desAuto=desAuto;
    this.precio=precio;
    this.pagoInicial=pagoInicial;
    this.plazo=plazo;
   
    }
    //copia
    public Cotizacion(Cotizacion otro){
        this.numCotizacion=otro.numCotizacion;
        this.desAuto=otro.desAuto;
        this.precio=otro.precio;
        this.pagoInicial=otro.pagoInicial;
        this.plazo=otro.plazo;
       
    }

    //metodos get/set
    
    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDesAuto() {
        return desAuto;
    }

    public void setDesAuto(String desAuto) {
        this.desAuto = desAuto;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPagoInicial() {
        return pagoInicial;
    }

    public void setPagoInicial(float pagoInicial) {
        this.pagoInicial = pagoInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    //metodos de comportamiento
    public float calcularPagoInicial(){
return precio*(pagoInicial/100);
}
    public float calcularTotal(){
    return precio-calcularPagoInicial(); 
    }
    public float calcularPagoMensual(){
    return calcularTotal()/plazo;
    }
}
