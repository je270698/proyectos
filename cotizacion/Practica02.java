/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cotizacion;
/**
 *
 * @author je270
 */
public class Practica02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //generar objeto por omisión
        Cotizacion cotizacionOmision;
        cotizacionOmision = new Cotizacion();
        cotizacionOmision.setNumCotizacion(1234);
        cotizacionOmision.setDesAuto("Yaris SX");
        cotizacionOmision.setPagoInicial(25);
        cotizacionOmision.setPrecio(220000);
        cotizacionOmision.setPlazo(36);
        
        
        
       
       System.out.println("Numero de cotización: " + cotizacionOmision.getNumCotizacion());
       System.out.println("Descripción del auto: " + cotizacionOmision.getDesAuto());
       System.out.println("Precio: " + cotizacionOmision.getPrecio());
       System.out.println("Porcentaje de pago inicial: " + cotizacionOmision.getPagoInicial());
       System.out.println("Plazo: " + cotizacionOmision.getPlazo());
       
       
       System.out.println("Pago inicial: "+cotizacionOmision.calcularPagoInicial());
       System.out.println("Total: "+cotizacionOmision.calcularTotal());
       System.out.println("Pago Mensual: "+cotizacionOmision.calcularPagoMensual());
    }
    
}
