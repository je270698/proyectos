/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package producto;

/**
 *
 * @author je270
 */
public class Producto {
    //atributos de la clase
    private int codProducto;
    private String descrip;
    private String unidadMedida;
    private float precioCompra;
    private float precioVenta;
    private int cantidadProductos;
//    //metodos
    //metodos constructores
    //metodos por omisión
    public Producto(){
        this.codProducto=0;
        this.descrip="";
        this.unidadMedida="";
        this.precioCompra=0.0f;
        this.precioVenta=0.0f;
        this.cantidadProductos=0;
    }
    //argumentos
    public Producto (int codProducto, String descrip, String unidadMedida, float precioCompra, float precioVenta, int cantidadProductos){
    this.codProducto=codProducto;
    this.descrip=descrip;
    this.unidadMedida=unidadMedida;
    this.precioCompra=precioCompra;
    this.precioVenta=precioVenta;
    this.cantidadProductos=cantidadProductos;
    }
    //copia
    public Producto (Producto otro){
    this.codProducto=otro.codProducto;
    this.descrip=otro.descrip;
    this.unidadMedida=otro.unidadMedida;
    this.precioCompra=otro.precioCompra;
    this.precioVenta=otro.precioVenta;
    this.cantidadProductos=otro.cantidadProductos;
    
    }

    //metodos get/set
    public int getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(int codProducto) {
        this.codProducto = codProducto;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public int getCantidadProductos() {
        return cantidadProductos;
    }

    public void setCantidadProductos(int cantidadProductos) {
        this.cantidadProductos = cantidadProductos;
    }
    public double calcularPrecioVenta(){
    return precioCompra * cantidadProductos * 1.5;
    }
    public double calcularPrecioCompra(){
    return precioVenta * cantidadProductos / 1.5;
    }
     public double calcularGanancia() {
        return calcularPrecioVenta() - calcularPrecioCompra();
    }
     
}
