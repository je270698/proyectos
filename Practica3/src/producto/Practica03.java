

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package producto;
/**
 *
 * @author je270
 */
public class Practica03 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Producto producto= new Producto();
        producto.setCantidadProductos(100);
        producto.setCodProducto(1203);
        producto.setDescrip("Atun de agua");
        producto.setUnidadMedida("piezas");
        producto.setPrecioCompra(10);
        producto.setPrecioVenta(15);
        
        System.out.println("Código del producto: "+producto.getCodProducto());
        System.out.println("Descripción: "+producto.getDescrip());
        System.out.println("Unidad de medida: "+producto.getUnidadMedida());
        System.out.println("Precio Compra: "+producto.getPrecioCompra());
        System.out.println("Precio venta: "+producto.getPrecioVenta());
        System.out.println("Cantidad de productos: "+producto.getCantidadProductos());
        System.out.println("Calculo Precio Venta: "+producto.calcularPrecioVenta());
        System.out.println("Calculo Precio Compra: "+producto.calcularPrecioCompra());
        System.out.println("Calculo de Ganancia: "+producto.calcularGanancia());
        
                
        
    }
    
}
