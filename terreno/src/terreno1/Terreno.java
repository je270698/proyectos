/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terreno1;
/**
 *
 * @author je270
es
 * and open the template in the editor.
 */


public class Terreno {
    
    //Atributos de la clase

private int numTerreno;
private float ancho;
private float largo;

//metodos
//Constructores

//Omision
public Terreno(){
	this.numTerreno=0;
	this.ancho=0.0f;
	this.largo=0.0f;
}

//Argumentos

public Terreno (int numTerreno, float ancho, float largo){
	this.numTerreno=numTerreno;
	this.ancho=ancho;
	this.largo=largo;
}
//copia

public Terreno (Terreno otro){
	this.numTerreno=otro.numTerreno;
	this.ancho=otro.ancho;
	this.largo=otro.largo;
}

//Metodos get/set

public int getNumTerreno(){
	return numTerreno;
}  

public void setNumTerreno(int numTerreno){
	this.numTerreno = numTerreno;
}
public float getAncho(){
	return ancho;
}

public void setAncho(float ancho){
	this.ancho = ancho;
}

public float getLargo(){
	return largo;
}  

public void setLargo(float largo){
	this.largo = largo;
}
//mMtodos de comportamiento
public float calcularPerimetro(){
	float perimetro = 0.0f;
	perimetro = (this.ancho + this.largo)*2;
	return perimetro;
}

public float calcularArea(){
	float area = 0.0f;
	area = this.ancho * this.largo;
	return area;
	} 
}