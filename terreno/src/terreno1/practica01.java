
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package terreno1;

/**
 *
 * @author je270
 */
public class practica01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Generar objeto construido por omision
	
	Terreno terreno = new Terreno();
	terreno.setAncho(10.50f);
	terreno.setLargo(20.00f);
	
	System.out.println("Perimetro = "+terreno.calcularPerimetro());
	System.out.println("Area = "+ terreno.calcularArea());

	Terreno ter = new Terreno(10,20.20f,40.00f);
	
	System.out.println("Lo ancho es "+ ter.getAncho());

	Terreno ter2 = new Terreno(terreno);

	System.out.println("Perimetro = "+ter2.calcularPerimetro());
	System.out.println("Area = "+ ter2.calcularArea());

    }     
    }